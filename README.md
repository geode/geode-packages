# GEODE packages

This repository provides [`guix`](https://guix.gnu.org/) packages for the code
used in project [GÉODE](https://geode-project.github.io/).

## How to use

The packages contained in this repository can be used in two ways with `guix`:
you can simply load the guile definitions it contains dynamically as you need
them, or you can add it to your known channels and have them appear as any
official `guix` package.

### Quick try

Assuming you are in a copy of this repository, commands like

```bash
guix search -L . python-stanza
```

should work and return results.

### Add the channel

To use this channel and get access to the packages it defines, add the following
block to your channels configuration file at `~/.config/guix/channels.scm`.

```guile
(channel
 (name 'geode)
 (url "https://gitlab.liris.cnrs.fr/geode/geode-packages")
 (branch "main"))
```

This file is meant to describe a list of channels, so the block above will
usually join other similar blocks as arguments to the `(cons* …)` function,
modifying the `%default-channels` value that is used when the file is missing.
Thus, if `geode` is the first channel you add to your `guix`, the whole file you
will create should look like:

```guile
(cons* (channel
         (name 'geode)
         (url "https://gitlab.liris.cnrs.fr/geode/geode-packages")
         (branch "main"))
       %default-channels)
```

## Disclaimer

This channel is a best-effort and a work in progress meant to allow analysis to
be done despite the fact that the quality of packages isn't perfect: some tests
are broken and have been deactivated, some descriptions are not properly
detailed.

## Contributing

Your contributions are most welcome. If you have an account on the LIRIS'
[gitlab instance](https://gitlab.liris.cnrs.fr/), just ask us for the rights.
Otherwise, send your `git` paches by email to the author of commit
`7e422c33b70409ecc47336b5583977f43669e45f`.
