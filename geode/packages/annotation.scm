(define-module (geode packages annotation)
  #:use-module ((gnu packages geo)
                #:select (python-geopandas))
  #:use-module ((gnu packages machine-learning)
                #:select (python-pytorch python-scikit-learn python-spacy))
  #:use-module ((gnu packages protobuf)
                #:select (python-protobuf))
  #:use-module ((gnu packages python-science)
                #:select (python-pandas))
  #:use-module ((gnu packages python-web)
                #:select (python-requests))
  #:use-module ((gnu packages python-xyz)
                #:select (python-check-manifest
                          python-emoji
                          python-folium
                          python-geojson
                          python-matplotlib
                          python-numpy
                          python-six
                          python-tqdm))
  #:use-module ((gnu packages xml)
                #:select (python-lxml))
  #:use-module ((guix build-system python)
                #:select (pypi-uri python-build-system))
  #:use-module ((guix download)
                #:select (url-fetch url-fetch/zipbomb))
  #:use-module ((guix gexp)
                #:select (gexp))
  #:use-module ((guix licenses)
                #:select (asl2.0 bsd-2 bsd-3 expat lgpl3))
  #:use-module ((guix packages)
                #:select (base32 origin package search-path-specification)))

(define-public python-gpxpy
  (package
    (name "python-gpxpy")
    (version "1.5.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "gpxpy" version))
              (sha256
               (base32
                "0247ddhrg42x1adlmgxjccni6g3cpj5vh16k7j1pmq7a8n4km6g6"))))
    (build-system python-build-system)
    (home-page "https://github.com/tkrajina/gpxpy")
    (synopsis "GPX file parser and GPS track manipulation library")
    (description "GPX file parser and GPS track manipulation library")
    (license asl2.0)))

(define-public python-perdido
  (package
    (name "python-perdido")
    (version "0.1.27")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "perdido" version))
              (sha256
               (base32
                "1j82mmaayv6ibxdbrhb44ryikh1bwc1h6jjp3nncyij3qlhvdbna"))))
    (build-system python-build-system)
    (arguments
     (list #:tests? #f
           #:phases #~(modify-phases %standard-phases
                        (delete 'sanity-check))))
    (propagated-inputs (list python-folium
                             python-geojson
                             python-geopandas
                             python-gpxpy
                             python-lxml
                             python-matplotlib
                             python-pandas
                             python-requests
                             python-scikit-learn
                             python-spacy))
    (home-page "https://github.com/ludovicmoncla/perdido")
    (synopsis "PERDIDO Geoparser python library")
    (description "PERDIDO Geoparser python library")
    (license bsd-2)))

(define-public python-stanza
  (package
    (name "python-stanza")
    (version "1.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "stanza" version))
              (sha256
               (base32
                "1wqhz82p158vhch3z3vglgy0s8qwr2aj83dm6y5qgkpbximc3l3h"))))
    (build-system python-build-system)
    (propagated-inputs (list python-emoji
                             python-numpy
                             python-protobuf
                             python-requests
                             python-six
                             python-pytorch
                             python-tqdm))
    (native-inputs (list python-check-manifest))
    (native-search-paths
     (list (search-path-specification
            (variable "STANZA_RESOURCES_DIR")
            (files '("lib/stanza_resources")))))
    (arguments
     `(#:tests? #f
       #:phases (modify-phases %standard-phases
                  (replace 'check
                    (lambda* (#:key inputs outputs tests? #:allow-other-keys)
                      (when tests?
                        (add-installed-pythonpath inputs outputs)
                        (invoke "python" "setup.py" "test")))))))
    (home-page "https://github.com/stanfordnlp/stanza")
    (synopsis
     "A Python NLP Library for Many Human Languages, by the Stanford NLP Group")
    (description
     "This package provides a Python NLP Library for Many Human Languages, by the
  Stanford NLP Group")
    (license asl2.0)))
