(define-module (geode packages demo)
  #:use-module ((geode packages annotation)
                #:select (python-perdido python-stanza))
  #:use-module ((geode packages models)
                #:select (spacy-fr-core-news-sm stanza-fr))
  #:use-module ((gnu packages base)
                #:select (coreutils))
  #:use-module ((gnu packages bash)
                #:select (bash))
  #:use-module ((gnu packages python)
                #:select (python))
  #:use-module ((gnu packages python-xyz)
                #:select (jupyter))
  #:use-module ((gnu packages wget)
                #:select (wget))
  #:use-module ((guix build-system copy)
                #:select (copy-build-system))
  #:use-module ((guix gexp)
                #:select (gexp))
  #:use-module ((guix git-download)
                #:select (git-fetch git-reference))
  #:use-module ((guix licenses)
                #:select (bsd-2))
  #:use-module ((guix packages)
                #:select (base32 origin package)))

(define-public geoparsing-tutorial
  (package
    (name "geoparsing-tutorial")
    (version "2fdf55058ca53a07265324fb23cd845650bc0010")
    (home-page
     "https://gitlab.liris.cnrs.fr/lmoncla/tutoriel-anf-tdm-2022-python-geoparsing")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url home-page)
                    (commit version)))
              (sha256
               (base32
                "0fp8b08z4y0l8g7ngwi057pmby27pw50ihc179p10zklp4v9n24h"))))
    (build-system copy-build-system)
    (propagated-inputs (list bash
                             coreutils
                             jupyter
                             python
                             python-perdido
                             python-spacy
                             python-stanza
                             spacy-fr-core-news-sm
                             stanza-fr
                             wget))
    (arguments
     (list #:install-plan #~'(("Tutoriel-geoparsing.ipynb"
                               "srv/notebooks/Tutoriel-geoparsing.ipynb")
                              ("data" "srv/notebooks/data"))))
    (synopsis
     "Tutorial comparing the use of three python library for geoparsing")
    (description
     "This package provides a notebook comparing the python libraries Perdido,
SpaCy and Stanza on geoparsing tasks.")
    (license bsd-2)))
