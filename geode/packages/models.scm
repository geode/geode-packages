(define-module (geode packages models)
  #:use-module ((guix build-system copy)
                #:select (copy-build-system))
  #:use-module ((guix build-system python)
                #:select (python-build-system))
  #:use-module ((guix download)
                #:select (url-fetch url-fetch/zipbomb))
  #:use-module ((guix gexp)
                #:select (gexp))
  #:use-module ((guix licenses)
                #:select (asl2.0 expat))
  #:use-module ((guix packages)
                #:select (base32 origin package)))

(define-public spacy-fr-core-news-sm
  (package
    (name "spacy-fr-core-news-sm")
    (version "3.4.0")
    (source (origin
              (method url-fetch)
              (uri
               "https://github.com/explosion/spacy-models/releases/download/fr_core_news_sm-3.4.0/fr_core_news_sm-3.4.0.tar.gz")
              (sha256
               (base32
                "1vq1lnvqiap25b9h38fa7hnv1n1xfx3703j4z2k1f56a0k4y5vbr"))))
    (build-system python-build-system)
    (arguments
     (list #:tests? #f
           #:phases #~(modify-phases %standard-phases
                        (delete 'sanity-check))))
    (home-page "https://spacy.io/models/fr")
    (synopsis "French model for SpaCy")
    (description "When they package their stuff correctly, maybe ?")
    (license expat)))

(define-public stanza-fr
  (package
    (name "stanza-fr")
    (version "1.3.0")
    (source (origin
              (method url-fetch/zipbomb)
              (uri (string-append
                    "https://huggingface.co/stanfordnlp/stanza-fr/resolve/v"
                    version "/models/default.zip"))
              (sha256
               (base32
                "0rjcgvwbxgxgbf3spf367p07bmycfkipn27n6qm63jbq35gx9b3d"))))
    (build-system copy-build-system)
    (propagated-inputs (list stanza-resources))
    (arguments
     (list #:install-plan #~'(("." "lib/stanza_resources/fr"))))
    (home-page "https://stanfordnlp.github.io/stanza/download_models.html")
    (synopsis "French model for Stanza")
    (description
     "This package provides a Python NLP Library for Many Human Languages, by
       the Stanford NLP Group")
    (license asl2.0)))

(define stanza-resources
  (package
    (name "stanza-resources")
    (version "1.3.0")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "https://downloads.cs.stanford.edu/nlp/software/stanza/stanza-resources/resources_"
                    version ".json"))
              (file-name "resources.json")
              (sha256
               (base32
                "00q6jw8sicbsp42dabx3w586jqb0hs4n2wz5c31cs6n5bz5aaa64"))))
    (build-system copy-build-system)
    (arguments
     (list #:install-plan #~'(("resources.json" "lib/stanza_resources/"))))
    (home-page "??")
    (synopsis "Metadata for the static resources for Stanza")
    (description "???")
    (license asl2.0)))
